#Comment to stop GitLab deleting this stuff

# Pigeon-OS

**King-Pigeon**

Converts any 64-bit (AMD64) Devuan supported OS to King-Pigeon (currently supported: Jessie, ASCII, Beowulf)

Features:

**SystemD block**

Apt has preferences set that mean any packages that rely on SystemD cannot be 'seen' or installed unintentionally by the user, helping with a systemD-free experience.

**LX Desktop Environment**

Configured to give that familiar classic XP look

**Unbloated**

King-Pigeon strips out all but the bare essentials from Devuan, allowing you to customise your OS how you see fit

**Customised Helpful Controls**

- Ctrl+Alt+Del brings up the LX Task Manager
- Ctrl+F1 enables a seamless area selection screenshot, with screenshots being automatically stored inside the screenshots folder, sorted into dynamically created screenshot folders for convenient searching


**Planned Features:**

*Offline OCR Reader*

Tired of manually typing out text from images? Don't want snoopy corporate third parties spying in? Us too! Pressing Ctrl+F4 calls up a local offline OCR tool to scan text from images. [ASCII English and Chinese only]

*Offline Voice Command*

Want to say 'Computer, do something!' but worried about privacy? Us too! Pressing Ctrl+F9 will activate voice command, with a customisable list of commands and phrases you want it to execute!

*Panic Shutdown*

Argh! Accidentally opened a billion tabs, RAM is exhausted and your boss has demanded a report in less than 5 minutes, and worried a forceful power off will damage the harddrive? No worries! Ctrl+Alt+F12 is 'panic shutdown', to forcefully shutdown the laptop no questions asked.
