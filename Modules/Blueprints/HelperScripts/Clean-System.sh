#!/bin/bash
#Version 1.0
#For clearing out the man page documentation in order to save space

#Clear out the thumbnail cache
rm -rf ~/.cache/thumbnails/*

path-exclude /usr/share/doc/*
path-exclude /usr/share/man/*
path-exclude /usr/share/groff/*
path-exclude /usr/share/info/*
path-exclude /usr/share/lintian/*
path-exclude /usr/share/linda/*

#Remove useless config files left behind for removed dpkg packages
dpkg --purge $(COLUMNS=200 dpkg -l | grep "^rc" | tr -s ' ' | cut -d ' ' -f 2)

