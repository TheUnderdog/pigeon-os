#!/bin/bash
#Version 1.0
#For cleaning out Devuan in preparation for whichever module is to run

_user=$(logname)

#GIMP is a nice program, but to trim off the fat, it has to go
apt-get purge -y gimp

#Get rid of bloat office packages
apt-get purge -y libreoffice
apt-get purge -y libreoffice-base
apt-get purge -y libreoffice-math
apt-get purge -y libreoffice-calc
apt-get purge -y libreoffice-writer
apt-get purge -y libreoffice-bin
apt-get purge -y libreoffice-draw
apt-get purge -y libreoffice-impress

#Get rid of the web browser
apt-get purge -y firefox-esr iceweasel w3m

#install a mini-web browser
apt-get install -y midori

#and printer support
apt-get purge -y hplip

#and more printer support
apt-get purge -y cups cups-browsed cups-bsd cups-client cups-common cups-core-drivers cups-daemon cups-filters cups-filters-core-drivers cups-pk-helper cups-ppdc cups-server-common

#get rid of their xarchiver
apt-get purge -y xarchiver xarchiver-debug

#Get rid of evince (for now)
apt-get purge -y evince-common

#and get rid of their music support
apt-get purge -y vlc

#and CD burning capabilities
apt-get purge -y xfburn

#and scanning abilities
apt-get purge -y xsane

#Eww, Vim
apt-get purge -y vim vim-tiny xxd

#What does this even do?!
apt-get purge -y texinfo

#Mousepad will be gone
apt-get purge -y mousepad

#Get rid of audio garbage
apt-get purge -y exfalso quodlibet

#Get rid of XFCE entirely
apt-get purge -y xfconf xfce4-utils xfwm4 xfce4-session thunar xfdesktop4 exo-utils xfce4-panel xfce4-terminal xfce4-taskmanager

#Get rid of the default display manager
apt-get purge -y slim

#Identified by deborphan as unneeded
apt-get purge -y btrfs-tools libprocps4

#Why would I want a command line emailer?
apt-get purge -y mutt

#Blergh, GNOME stuff
apt-get purge -y libgtk-3-0 gtk-update-icon-cache libfm-gtk-data gnome-orca

#Blergh, Wayland reliant crap
apt-get purge -y libegl1-mesa libgbm1

#Blergh, Wayland itself
dpkg --purge libwayland-client0
dpkg --purge libwayland-server0

#Blergh, insecure shit
apt-get purge -y bluetooth bluez

#Devuan, Y U contain so much documentation?!
apt-get purge -y debian-faq doc-debian live-boot-doc live-config-doc

#Blergh, surveillance state
apt-get purge -y geoip-database

#Now to rip out the systemd surrogate
apt-get purge -y libsystemd0

#Clear out useless xfce4 folder
rm -rf /etc/xdg/xfce4

#Clear out useless Thundar folder
rm -rf /etc/xdg/Thundar

#Purge the usr/share folder containing the XFCE4 junk we no longer need, which isn't removed when XFCE goes
rm -rf "/usr/share/xfce4"

#Remove everything in the background images base
rm /usr/share/images/desktop-base/*

#Call the script to clear out man-pages and other unnecessary documentation, files
bash "/home/$_user/Modules/HelperScripts/Clear-System.sh"

#Finally, get rid of everything for realsies
apt-get autoremove -y

