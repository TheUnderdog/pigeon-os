#!/bin/bash
#Version 2.0
#Assumes the target system it's being deployed on is Devuan Jessie with intention to upgrade to ASCII

#Do a sudo/root level check
uid=$(id -u)

#We don't have root permissions, so we can't install or upgrade anything, so we quit
[ $uid -ne 0 ] && { echo "Only root may run this Blueprint installation package."; exit 1; }

#run our first update
apt-get update

#Check for UFW
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' ufw|grep "install ok installed")

#It's not there, install and enable it
if [ "" == "$PKG_OK" ]; then
 	apt-get --force-yes --yes install ufw
	#Enable the firewall
	ufw enable
fi

_user=$(logname)

#Delete the garbage files off the desktop
rm "/home/$_user/Desktop/_RELEASE_NOTES"
rm "/home/$_user/Desktop/SMALLER_FONTS.desktop"
rm "/home/$_user/Desktop/LARGER_FONTS.desktop"

#Navigate to the new directory
cd "/home/$_user/Modules"

#Transfer a copy of the sources.list file (which should be correctly configured)
#cp  /etc/apt/sources.list /etc/apt/jessie-sources.list.bak
#cp  "/home/$_user/Modules/Blueprints/HelperScripts/Replacements/jessie-sources.list" /etc/apt/sources.list

#Now we've updated the sources.list file to include everything, update our own cache
#apt-get update

#Clean out Devuan
#bash "/home/$_user/Modules/Blueprints/HelperScripts/Clean-Devuan.sh"


#Key signing manager used by Trinity desktop
apt-get install -y dirmngr

apt-key adv --keyserver keyserver.quickbuild.io --recv-keys F5CFC95C

#Install the Trinity desktop
#apt-get install -y tde-trinity

#Install the wifi gui
apt-get install wpagui


#bring in our preferred file archiving program
#apt-get install -y file-roller



#Add the King Pigeon Background
mkdir -p "/usr/share/images/desktop-base/"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/King-Pigeon-Space.png" /usr/share/images/desktop-base/

#Add the King Pigeon icon
mkdir -p "/usr/share/pixmaps"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/King-Pigeon-Logo-Mini.png" /usr/share/pixmaps

#Set up the login screen's details
mkdir -p /etc/lightdm
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/lightdm-gtk-greeter.conf" /etc/lightdm

#Update the openbox rc file
mkdir -p "/home/$_user/.config/openbox/"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/lxde-rc.xml" "/home/$_user/.config/openbox/"

#Copy over the menu logo
mkdir -p "/usr/share/lxde/images/"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/King-Pigeon-Logo-Mini-Simple-2b-Menu.png" "/usr/share/lxde/images/"

#Copy over the panel configuration to make things look nice
mkdir -p "/home/$_user/.config/lxpanel/LXDE/panels/"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/panel" "/home/$_user/.config/lxpanel/LXDE/panels/"

#Copy over the logout banner
mkdir -p "/usr/share/lxde/images/"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/logout-banner.png" "/usr/share/lxde/images/"

#Copy over the background desktop setup
mkdir -p "/home/$_user/.config/pcmanfm/LXDE"
cp  "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/desktop-items-0.conf" "/home/$_user/.config/pcmanfm/LXDE"

#Non-UEFI users use grub-pc
#Installed UEFI grub support
#apt-get install -y grub-efi-amd64

#Remove unnecessary grub support
#apt-get purge -y grub-efi-ia32-bin

#Install offline apt support
apt-get install -y apt-offline

mkdir -p "/etc/apt/preferences.d"
cp "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/avoid-libsystemd0" "/etc/apt/preferences.d"
cp "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/avoid-systemd-container" "/etc/apt/preferences.d"
cp "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/avoid-libsystemd-dev" "/etc/apt/preferences.d"
cp "/home/$_user/Modules/Blueprints/King-Pigeon/Replacements/avoid-systemd-coredump" "/etc/apt/preferences.d"

#Clean out the archive space of debian packages to free up disk space
apt-get clean

#Clean out the removed packages fully
apt-get autoremove -y

#Update the .img file pre-emptively for the snapshot
#update-initramfs -u

#Reboot so the changes take effect
#reboot
